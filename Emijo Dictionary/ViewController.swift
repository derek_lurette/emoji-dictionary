//
//  ViewController.swift
//  Emijo Dictionary
//
//  Created by Derek Lurette on 2017-11-02.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var emojiList: UITableView!
    
    var emojis : [Emoji] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        emojiList.dataSource = self
        emojiList.delegate = self
        emojis = makeEmojiArray()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emojis.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let emoji = emojis[indexPath.row]
        cell.textLabel?.text = emoji.stringEmoji
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let emoji = emojis[indexPath.row]
        performSegue(withIdentifier: "MoveSegue", sender: emoji)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let definitionVC = segue.destination as! DefinitionViewController
        definitionVC.emoji = sender as! Emoji
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func makeEmojiArray() -> [Emoji] {
        let emoji1 = Emoji()
        emoji1.stringEmoji = "🤪"
        emoji1.definition = "Goofy Face"
        emoji1.category = "Smiley Faces"
        emoji1.birthYear = 2010
        
        let emoji2 = Emoji()
        emoji2.stringEmoji = "💩"
        emoji2.definition = "Poop Face"
        emoji2.category = "Smiley Faces"
        emoji2.birthYear = 2007
        
        let emoji3 = Emoji()
        emoji3.stringEmoji = "🍺"
        emoji3.definition = "Beer"
        emoji3.category = "Things"
        emoji3.birthYear = 2014
        
        let emoji4 = Emoji()
        emoji4.stringEmoji = "🐶"
        emoji4.definition = "Dog"
        emoji4.category = "Animals"
        emoji4.birthYear = 2003
        
        return [emoji1, emoji2, emoji3, emoji4]
    }
    
}

