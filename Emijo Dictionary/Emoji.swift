//
//  Emoji.swift
//  Emijo Dictionary
//
//  Created by Derek Lurette on 2017-11-03.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

class Emoji {
    var stringEmoji = ""
    var definition = ""
    var category = ""
    var birthYear = 0
}
